// =============================================================================
//
// Waybar configuration
//
// Configuration reference: https://github.com/Alexays/Waybar/wiki/Configuration
//
// =============================================================================

{
    // -------------------------------------------------------------------------
    // Global configuration
    // -------------------------------------------------------------------------

    "layer": "top",

    "position": "top",

    // If height property would be not present, it'd be calculated dynamically
    "height": 30,

    "modules-left": [
        "sway/workspaces",
        "sway/mode"
    ],
    "modules-center": [
        "sway/window"
    ],
    "modules-right": [
        "network",
        "bluetooth",
        "cpu",
        "memory",
        "battery",
        "backlight",
        "idle_inhibitor",
        "pulseaudio",
        "custom/pacman",
        "tray",
        "clock"
    ],


    // -------------------------------------------------------------------------
    // Modules
    // -------------------------------------------------------------------------

    "battery": {
        "interval": 30,
        "states": {
            "warning": 30,
            "critical": 15
        },
        "format-charging": " {icon} {capacity}%", // Icon: bolt
        "format": "{icon} {capacity}%",
        "format-icons": [
            "", // Icon: battery-empty
            "", // Icon: battery-quarter
            "", // Icon: battery-half
            "", // Icon: battery-three-quarters
            ""  // Icon: battery-full
        ],
        "tooltip": true,
        "on-click": "powersupply"
    },

    "clock": {
        "interval": 60,
        "format": " {:%e %b %Y %H:%M}", // Icon: calendar-alt
        "tooltip": true,
        "tooltip-format": "<big>{:%B %Y}</big>\n<tt>{calendar}</tt>"
    },

    "cpu": {
        "interval": 5,
        "format": " {usage}%", // Icon: microchip
        "states": {
            "warning": 70,
            "critical": 90
        },
        "on-click": "swaymsg exec \\$term_float_landscape bpytop"
    },

    "memory": {
        "interval": 5,
        "format": " {}%", // Icon: memory
        "states": {
            "warning": 70,
            "critical": 90
        },
        "on-click": "swaymsg exec \\$term_float_landscape bpytop"
    },

    "network": {
        "interval": 5,
        "format-wifi": " {essid} ({signalStrength}%)", // Icon: wifi
        "format-ethernet": " {ifname}", // Icon: ethernet
        "format-disconnected": "⚠ Disconnected",
        "tooltip-format": "{ifname}: {ipaddr}",
        "on-click": "swaymsg exec \\$term_float_portrait nmtui"
    },

    "sway/mode": {
        "format": "<span style=\"italic\">{}</span>",
        "tooltip": false
    },

    "sway/window": {
        "format": "{}",
        "max-length": 120
    },

    "sway/workspaces": {
        "all-outputs": false,
        "disable-scroll": true,
        "format": "{name}"
    },

    "backlight": {
        "format": "{icon} {percent}%",
        "format-icons": ["", ""],
        "on-scroll-down": "light -A 1",
        "on-scroll-up": "light -U 1"
    },

    "idle_inhibitor": {
        "format": "{icon}",
        "format-icons": {
            "activated": "",
            "deactivated": ""
        }
    },

    "pulseaudio": {
        //"scroll-step": 1,
        "format": "{icon} {volume}%   {format_source}",
        "format-bluetooth": "{icon} {volume}%",
        "format-muted": "",
        "format-icons": {
            "headphones": "",
            "handsfree": "",
            "headset": "",
            "phone": "",
            "portable": "",
            "car": "",
            "default": ["", ""]
        },
        "on-click": "pavucontrol"
    },

    "temperature": {
      "critical-threshold": 90,
      "interval": 5,
      "format": "{icon} {temperatureC}°C",
      "format-icons": [
          "", // Icon: temperature-empty
          "", // Icon: temperature-quarter
          "", // Icon: temperature-half
          "", // Icon: temperature-three-quarters
          ""  // Icon: temperature-full
      ],
      "tooltip": true
    },

    "tray": {
        "icon-size": 21,
        "spacing": 5
    },

    "custom/pacman": {
        "format": " {}",
        "interval": 3600,
        "exec-if": "[ $(checkupdates | wc -l) -gt 0 ]",
        "exec": "pamac checkupdates | wc -l",
        "on-click": "pamac-manager --updates",
    },

    "bluetooth": {
        "format": "{icon}",
        "interval": 30,
        "format-icons": {
            "enabled": "",
            "disabled": ""
        },
        "on-click": "blueman-manager",
        "tooltip-format": "{status}"
    }
}
